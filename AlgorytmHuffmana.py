# -*- coding: utf-8 -*-
# Wczytanie pliku txt
from collections import defaultdict
import heapq
import sys

f = open("C:\\Users\\Magnum34\\Desktop\\przetwarzanie_multimediow\\tekst.txt",'r')
txt = f.read()

words = defaultdict(int)

for char in txt:
    words[char] += 1
def huffmana(words):
	heap = [[wt, [sym, ""]] for sym, wt in words.items()]
	#tworzenie kopca poprzez heapify, sortowanie
	heapq.heapify(heap)
	#Kodowanie Hoffmana
	while len(heap) > 1:
		left = heapq.heappop(heap)
		right = heapq.heappop(heap)
		for binar in left[1:]:
			binar[1] = '0'+binar[1]
		for binar in right[1:]:
			binar[1] = '1'+binar[1]
		#tworzenie drzewa hoffmana
		heapq.heappush(heap, [left[0]+right[0]]+left[1:]+right[1:])
	table = heapq.heappop(heap)[1:]
	return table
tab = huffmana(words)
for alphabet,binary in tab:
	print "%s\t%s"%(alphabet,binary)

